package org.springframework.samples.petclinic.caching;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class SimpleCacheTests {
    @Mock
    Role role;

    @Spy
    EntityManager em;

    @InjectMocks
    SimpleCache sc;

    @Test
    @Ignore // test fails because the cache always misses.
    public void retrieveOneObjectTwiceShouldMakeAHitOnCache() throws Exception {
        when(role.getName()).thenReturn("assistant");

        sc.retrieve(role.getClass(), role);
        verify(em, times(1)).find(role.getClass(), role);

        sc.retrieve(role.getClass(), role);
        verify(em, times(1)).find(role.getClass(), role);
    }

    @Test
    public void evictShouldRemoveItemFromCache() throws Exception {
        when(role.getName()).thenReturn("assistant");

        sc.retrieve(role.getClass(), role);
        verify(em, times(1)).find(role.getClass(), role);

        sc.evict(role.getClass(), role);
        sc.retrieve(role.getClass(), role);
        verify(em, times(2)).find(role.getClass(), role);
    }
}