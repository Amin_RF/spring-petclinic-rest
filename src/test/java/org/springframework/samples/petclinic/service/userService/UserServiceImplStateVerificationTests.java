package org.springframework.samples.petclinic.service.userService;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.service.UserService;
import org.springframework.samples.petclinic.util.UserServiceConfig;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;
import java.util.HashSet;

@RunWith(SpringRunner.class)
@Service
@ContextConfiguration(classes = { UserServiceConfig.class }, loader = AnnotationConfigContextLoader.class)
@ActiveProfiles("test")
public class UserServiceImplStateVerificationTests { 
    @Mock
    private User user;
    
    @Rule
    public ExpectedException ee = ExpectedException.none();
    
    @Spy
    private Role role;

    @Spy
    private Role roleWithPrefix;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService us;

    @Test
    public void saveUserWithNoRolesShouldThrowExceptionTest() throws Exception {
        when(user.getRoles()).thenReturn(null);
        ee.expect(Exception.class);

        us.saveUser(user);
    }

    @Test
    public void saveUserWithRoleNotHavingPrefixShouldAddItAndLeavePrefixedRoleAlone() throws Exception {
        setTestRoleList();

        us.saveUser(user);
        assertEquals("ROLE_assistant", role.getName());
        assertEquals("ROLE_worker", roleWithPrefix.getName());
    }

    @Test
    public void saveUserWithARoleWithoutUserShouldSetUser() throws Exception {
        setTestRoleList();
        roleWithPrefix.setUser(null);

        us.saveUser(user);
        assertEquals(user, role.getUser());
    }

    @Test
    public void saveUserWithValidRoleShouldAddUserToRepository() throws Exception {
        setTestRoleList();

        int usersSize = ((UserServiceConfig.UserRepositoryStub)userRepository).getUsersSize();
        us.saveUser(user);
        assertEquals(usersSize + 1, ((UserServiceConfig.UserRepositoryStub)userRepository).getUsersSize());
    }

    public void setTestRoleList() {
        String roleName = "assistant";
        String roleNamePrefixed = "ROLE_worker";

        HashSet<Role> roles = new HashSet<>();
        role.setName(roleName);
        roleWithPrefix.setName(roleNamePrefixed);
        roles.add(role);
        roles.add(roleWithPrefix);
        when(user.getRoles()).thenReturn(roles);
    }
}
