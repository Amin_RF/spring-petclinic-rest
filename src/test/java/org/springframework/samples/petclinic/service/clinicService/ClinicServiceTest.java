package org.springframework.samples.petclinic.service.clinicService;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

import com.github.mryf323.tractatus.CACC;
import com.github.mryf323.tractatus.ClauseCoverage;
import com.github.mryf323.tractatus.NearFalsePoint;
import com.github.mryf323.tractatus.UniqueTruePoint;
import com.github.mryf323.tractatus.Valuation;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.samples.petclinic.model.Owner;
import org.springframework.samples.petclinic.model.Pet;
import org.springframework.samples.petclinic.model.PetType;
import org.springframework.samples.petclinic.model.Specialty;
import org.springframework.samples.petclinic.model.Vet;
import org.springframework.samples.petclinic.model.Visit;
import org.springframework.samples.petclinic.repository.PetRepository;
import org.springframework.samples.petclinic.repository.VetRepository;
import org.springframework.samples.petclinic.repository.VisitRepository;
import org.springframework.samples.petclinic.service.ClinicServiceImpl;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class ClinicServiceTest {
    // necessary stubs
    @Mock PetRepository petr;
    @Mock VetRepository vetr;
    @Mock VisitRepository visr;
    @InjectMocks ClinicServiceImpl cs;
    
    // temp stubs
    @Mock Specialty sp;
    @Mock Pet pet;
    @Spy Vet vet;
    @Spy Visit vis;
    @Spy PetType pt;

    @Rule public ExpectedException ee = ExpectedException.none();
    
    private ArrayList<Pet> pets;
    private ArrayList<Vet> vets;
    private Owner owner;
    
    @Before
    public void init() {
        pets = new ArrayList<>();
        vets = new ArrayList<>();
        vis = new Visit();
        
        vet = new Vet();
        vet.addSpecialty(sp);
        vets.add(vet);
        
        // init pets
        pt = new PetType();
        pt.setName("petType");
        when(pet.getType()).thenReturn(pt);

        when(vetr.findAll()).thenReturn(vets);
    }

    private void initEmpty() {
        initPets();
        when(pet.getLastVisit()).thenReturn(Optional.empty());
    }

    private void initFull() {
        initPets();
        when(pet.getLastVisit()).thenReturn(Optional.of(vis));
    }

    private void initPets() {
        pets.add(pet);
        owner = new Owner();
        when(petr.findByOwner(owner)).thenReturn(pets);        
    }

    private void initSp(boolean state) {
        when(sp.canCure(pt)).thenReturn(state);
    }

    @ClauseCoverage(
        // a -> last.isPresent()
        // a -> notVisited.size() > 0
        // a -> pets.size()
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )
    @CACC(
        // a -> last.isPresent()
        // a -> notVisited.size() > 0
        // a -> pets.size()
        predicate = "a",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        },
        predicateValue = false
    )
    @NearFalsePoint(
        // a -> last.isPresent()
        // a -> notVisited.size() > 0
        // a -> pets.size()
        predicate = "a",
        cnf = "a",
        clause = 'a',
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )
    @Test
    public void visitOwnerPetsWithoutPets() {
        cs.visitOwnerPets(owner);
    }

    @CACC(
        // a -> age > 3
        // b -> daysFromLastVisit > 364
        // c -> age <= 3
        // d -> daysFromLastVisit > 182
        predicate = "(a && b) || (c && d)",
        majorClause = 'b',
        valuations = {
            @Valuation(clause = 'a', valuation = true),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = false),
        },
        predicateValue = false
    )
    @NearFalsePoint(
        // a -> age > 3
        // b -> daysFromLastVisit > 364
        // c -> age <= 3
        // d -> daysFromLastVisit > 182
        predicate = "(a && b) || (c && d)",
        cnf = "(a && b) || (c && d)",
        implicant = "cd",
        clause = 'b',
        valuations = {
            @Valuation(clause = 'a', valuation = true),
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = false),
        }
    )
    @Test
    public void visitOwnerPetsLastPresentAndNotVisitPet() {
        initFull();
        initSp(true);

        java.util.Date dob = java.sql.Date.valueOf(LocalDate.now().minusYears(4));
        when(pet.getBirthDate()).thenReturn(dob);

        java.util.Date visd = java.sql.Date.valueOf(LocalDate.now().minusDays(1));
        vis.setDate(visd);

        cs.visitOwnerPets(owner);
        verify(visr, times(0)).save(any(Visit.class));
    }

    @ClauseCoverage(
        // a -> last.isPresent()
        // a -> pets.size()
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true),
        }
    )
    @ClauseCoverage(
        // a -> age > 3
        // b -> daysFromLastVisit > 364
        // c -> age <= 3
        // d -> daysFromLastVisit > 182
        predicate = "(a && b) || (c && d)",
        valuations = {
            @Valuation(clause = 'a', valuation = true),
            @Valuation(clause = 'b', valuation = true),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = true),
        }
    )
    @CACC(
        // a -> last.isPresent()
        // a -> pets.size()
        predicate = "a",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        },
        predicateValue = true
    )
    @UniqueTruePoint(
        // a -> last.isPresent()
        // a -> pets.size()
        predicate = "a",
        cnf = "a",
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )
    @Test
    public void visitOwnerPetsLastPresentAndVisitPet() {
        initFull();
        initSp(true);
        
        java.util.Date dob = java.sql.Date.valueOf(LocalDate.now().minusYears(4));
        when(pet.getBirthDate()).thenReturn(dob);
        vis.setDate(dob);

        cs.visitOwnerPets(owner);
        verify(visr).save(any(Visit.class));
    }

    @ClauseCoverage(
        // a -> notVisited.size()
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )
    @CACC(
        // a -> notVisited.size()
        predicate = "a",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        },
        predicateValue = true
    )
    @UniqueTruePoint(
        // a -> notVisited.size()
        predicate = "a",
        cnf = "a",
        implicant = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )
    @Test
    public void visitOwnerPetsNotLastPresentAndNotVisited() {
        initEmpty();
        initSp(false);

        ee.expect(ClinicServiceImpl.VisitException.class);
        cs.visitOwnerPets(owner);
    }

}