package org.springframework.samples.petclinic.service.userService;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.service.UserServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@Service
public class UserServiceImplBehaviouralVerificationTests {

    @Rule
    public ExpectedException ee = ExpectedException.none();

    @Captor
    ArgumentCaptor<String> args;

    @Mock
    private Role role;

    @Mock
    private Role roleWithPrefix;

    @Mock
    private User user;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl us;

    @Test
    public void saveUserWithNoRolesShouldNotSaveUser() throws Exception {
        when(user.getRoles()).thenReturn(null);
        ee.expect(Exception.class);

        us.saveUser(user);

        verify(user, times(1)).getRoles();
        verify(us, times(1)).saveUser(user);
        verify(userRepository, times(0)).save(user);
    }

    @Test
    public void saveUserWithRoleNotHavingPrefixShouldAddIt() throws Exception {
        setTestRoleList();

        us.saveUser(user);
        
        verify(user, times(3)).getRoles();
        verify(role, times(1)).setName(args.capture());
        assertEquals("ROLE_assistant", args.getValue());
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void saveUserWithARoleWithoutUserShouldSetUser() throws Exception {
        setTestRoleList();
        when(role.getUser()).thenReturn(null);

        us.saveUser(user);

        verify(role, times(1)).setUser(user);
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void saveUserWithARoleStartingWithPrefixAndARoleNotStartingWithPrefixShouldSetPrefixToLatterAndLeaveTheFormerAlone() throws Exception {
        setTestRoleList();
        when(roleWithPrefix.getUser()).thenReturn(user);

        us.saveUser(user);

        verify(user, times(3)).getRoles();
        verify(role, times(1)).setName(args.capture());
        assertEquals("ROLE_assistant", args.getValue());

        verify(roleWithPrefix, times(0)).setName("ROLE_worker");

        verify(userRepository, times(1)).save(user);
    }

    public void setTestRoleList() {
        String roleName = "assistant";
        String roleNamePrefixed = "ROLE_worker";

        HashSet<Role> roles = new HashSet<>();
        when(role.getName()).thenReturn(roleName);
        when(roleWithPrefix.getName()).thenReturn(roleNamePrefixed);
        roles.add(role);
        roles.add(roleWithPrefix);
        when(user.getRoles()).thenReturn(roles);
    }
}
