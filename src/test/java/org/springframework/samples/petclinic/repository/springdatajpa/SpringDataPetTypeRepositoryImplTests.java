package org.springframework.samples.petclinic.repository.springdatajpa;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.samples.petclinic.model.Pet;
import org.springframework.samples.petclinic.model.PetType;
import org.springframework.samples.petclinic.model.Visit;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Iman Sahebi
 *
 */

@RunWith(SpringRunner.class)
@Repository
public class SpringDataPetTypeRepositoryImplTests {

    @Spy
    private PetType pt;
    
    @Mock
    private Pet pet;
    
    @Mock
    private Visit visit;
    
    private List<Pet> pets;
    private List<Visit> visits;

    @Mock
    private EntityManager em;

    @Mock
    private Query q;

    @InjectMocks
    private SpringDataPetTypeRepositoryImpl repo;

    @Before
    public void clear() throws Exception {
        pets = new ArrayList<>();
        visits = new ArrayList<>();
    }

    public void initiatePetTypes() throws Exception {
        pt = new PetType();
        pt.setName("Leo");
        pt.setId(1);
    }

    public void initiateQueries() throws Exception {
        when(em.createQuery(any(String.class))).thenReturn(q);
        when(q.getResultList()).thenReturn(pets);
        when(q.executeUpdate()).thenReturn(0);
    }

    public void initiateEmptyPets() throws Exception {
        pets = new ArrayList<Pet>();
    }

    public void initiateOnePets() throws Exception {
        when(pet.getVisits()).thenReturn(visits);
        pets = new ArrayList<Pet>() {{
            add(pet);
        }};
    }

    public void initiateOnePetsWithVisits() throws Exception {
        when(visit.getId()).thenReturn(2);
        visits.add(visit);
        when(pet.getVisits()).thenReturn(visits);

        pets = new ArrayList<Pet>() {{
            add(pet);
        }};
    }

    public void initiateObjects(int mode) throws Exception {
        if (mode == 0)
            initiateEmptyPets();
        else if (mode == 1)
            initiateOnePets();
        else if (mode == 2)
            initiateOnePetsWithVisits();
        initiatePetTypes();
        initiateQueries();
    }

    // tests that not contain petType
    @Test
    public void TestNotContainWithNoPets() throws Exception {
        initiateObjects(0);
        when(em.contains(pt)).thenReturn(false);
        when(em.merge(pt)).thenReturn(pt);

        repo.delete(pt);

        verify(em, times(1)).merge(pt);
        verify(q, times(1)).executeUpdate();
    }

    @Test
    public void TestContainWithPetsWithoutVisits() throws Exception {
        initiateObjects(1);
        when(em.contains(pt)).thenReturn(false);
        when(em.merge(pt)).thenReturn(pt);

        repo.delete(pt);

        verify(pet, times(1)).getVisits();
        verify(q, times(2)).executeUpdate();
    }

    @Test
    public void TestContainWithPetsWithVisits() throws Exception {
        initiateObjects(2);
        when(em.contains(pt)).thenReturn(false);
        when(em.merge(pt)).thenReturn(pt);

        repo.delete(pt);

        verify(pet, times(1)).getVisits();
        verify(visit, times(1)).getId();
        verify(q, times(3)).executeUpdate();
    }

    // tests that contain petType
    @Test
    public void TestWithNoPets() throws Exception {
        initiateObjects(0);
        when(em.contains(pt)).thenReturn(true);

        repo.delete(pt);

        verify(em, times(0)).merge(pt);
    }

    @Test
    public void TestWithPetsWithoutVisits() throws Exception {
        initiateObjects(1);
        when(em.contains(pt)).thenReturn(true);

        repo.delete(pt);

        verify(pet, times(1)).getVisits();
        verify(q, times(2)).executeUpdate();
    }

    @Test
    public void TestWithPetsWithVisits() throws Exception {
        initiateObjects(2);
        when(em.contains(pt)).thenReturn(true);

        repo.delete(pt);

        verify(pet, times(1)).getVisits();
        verify(visit, times(1)).getId();
        verify(q, times(3)).executeUpdate();
    }
}
