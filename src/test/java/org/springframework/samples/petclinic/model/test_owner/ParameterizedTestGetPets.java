package org.springframework.samples.petclinic.model.test_owner;

import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.samples.petclinic.model.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.*;

@RunWith(Parameterized.class)
public class ParameterizedTestGetPets {
    private List<String> expected_pets_name;

    private Owner owner;

    public ParameterizedTestGetPets(String[] owner_pets_name, String[] expected_pets_name){
        this.owner = new Owner();
        for(String pet_name : owner_pets_name){
            Pet pet = new Pet();
            pet.setName(pet_name);
            this.owner.addPet(pet);
        }
        this.expected_pets_name = Arrays.asList(expected_pets_name);
    }


    @Parameters public static Collection<String[][]> parameters()
    {
        return Arrays.asList(new String[][][]
            {
                {{"jack", "maly", "anderson"}, {"anderson", "jack", "maly"}},
                {{"abnabat", "shokolat"}, {"abnabat", "shokolat"}},
                {{"jigar", "bahram"}, {"bahram", "jigar"}},
            }
        );
    }

    @Test public void test_get_pets_sort()
    {
        List<String> pets_name = new ArrayList<>();
        for(Pet pet : this.owner.getPets())
        {
            pets_name.add(pet.getName());
        }
        assertEquals(this.expected_pets_name, pets_name);
    }
}
