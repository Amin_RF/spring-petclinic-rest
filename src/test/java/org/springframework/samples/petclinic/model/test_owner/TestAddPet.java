package org.springframework.samples.petclinic.model.test_owner;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.samples.petclinic.model.*;



public class TestAddPet {
    private Owner owner = null;
    @Before public void create_user(){
        this.owner = new Owner();
    }
    @Test public void test_add_pet()
    {
        Pet pet = new Pet();
        this.owner.addPet(pet);
        assertEquals(1, this.owner.getPets().size());
        assertEquals(this.owner, pet.getOwner());
    }
    @Test public void test_add_duplicated_pet() {
        Pet pet = new Pet();
        this.owner.addPet(pet);
        this.owner.addPet(pet);
        assertEquals(1, this.owner.getPets().size());
    }

}
