package org.springframework.samples.petclinic.model.test_owner;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.Assume.*;

import org.junit.experimental.theories.*;
import org.springframework.samples.petclinic.model.*;
import org.junit.runner.RunWith;

import java.util.*;

@RunWith(Theories.class)
public class TheoryTestGetPet {

    private Owner owner;


    @DataPoints("pets")
    public static Pet[] get_pets()
    {
        Pet[] pets = new Pet[3];
        pets[0] = new Pet();
        pets[0].setName("first_pet");
        pets[0].setId(0);
        pets[1] = new Pet();
        pets[1].setName("second_pet");
        pets[2] = new Pet();
        pets[2].setName("third_pet");
        return pets;
    }


    @Before
    public void create_owner()
    {
        this.owner = new Owner();
        this.owner.setFirstName("amin");
        Pet[] pet_array = TheoryTestGetPet.get_pets();
        for(Pet pet : pet_array){
            this.owner.addPet(pet);
        }
    }

    @Theory
    public void test_ignore_new(@FromDataPoints("pets") Pet pet)
    {
        assumeTrue(pet.isNew());
        assertNull(this.owner.getPet(pet.getName(), true));
    }

    @Theory
    public void test_new_pet(@FromDataPoints("pets") Pet pet)
    {
        assumeTrue(pet.isNew());
        assertNotNull(this.owner.getPet(pet.getName(), false));
    }

    @Theory
    public void test_not_new_pet(@FromDataPoints("pets") Pet pet)
    {
        assumeFalse(pet.isNew());
        assertNotNull(this.owner.getPet(pet.getName(), true));
        assertNotNull(this.owner.getPet(pet.getName(), false));
    }
}
