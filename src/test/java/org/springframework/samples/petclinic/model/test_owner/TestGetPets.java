package org.springframework.samples.petclinic.model.test_owner;

import org.junit.*;
import org.junit.Assert.*;
import org.springframework.samples.petclinic.model.Owner;
import org.springframework.samples.petclinic.model.Pet;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestGetPets {

    private Owner owner = null;
    private Pet my_first_pet = null;
    private Pet my_second_pet = null;

    @Before public void create_owner_and_pets()
    {
        this.my_first_pet = new Pet();
        this.my_first_pet.setName("1st_pet");
        this.my_first_pet.setId(1);
        this.my_second_pet = new Pet();
        this.my_second_pet.setName("2nd_pet");
        this.owner = new Owner();
        this.owner.addPet(this.my_first_pet);
        this.owner.addPet(this.my_second_pet);
    }

    @Test public void test_happy_path()
    {
        List<Pet> pets_list = this.owner.getPets();
        assertEquals(this.my_first_pet, pets_list.get(0));
        assertEquals(this.my_second_pet, pets_list.get(1));
        assertEquals(2, this.owner.getPets().size());
    }

    @Test public void test_change_name()
    {
        this.my_first_pet.setName("2nd_pet");
        this.my_second_pet.setName("1st_pet");
        List<Pet> pets_list = this.owner.getPets();
        assertEquals(this.my_first_pet, pets_list.get(1));
    }

    @Test public void test_add_new_pet()
    {
        Pet new_pet = new Pet();
        this.owner.addPet(new_pet);
        assertEquals(3, this.owner.getPets().size());
    }
}
