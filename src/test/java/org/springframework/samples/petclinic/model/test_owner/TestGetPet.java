package org.springframework.samples.petclinic.model.test_owner;

import org.junit.Before;
import org.junit.Test;
import org.springframework.samples.petclinic.model.Owner;
import org.springframework.samples.petclinic.model.Pet;

import static org.junit.Assert.*;

public class TestGetPet {

    private Owner owner = null;
    private Pet my_first_pet = null;
    private Pet my_second_pet = null;

    @Before public void create_owner_and_pets()
    {
        this.my_first_pet = new Pet();
        this.my_first_pet.setName("my_first_pet");
        this.my_first_pet.setId(1);
        this.my_second_pet = new Pet();
        this.my_second_pet.setName("my_second_pet");
        this.owner = new Owner();
        this.owner.addPet(this.my_first_pet);
        this.owner.addPet(this.my_second_pet);
    }
    @Test public void test_happy_path()
    {
        assertEquals(this.my_first_pet, this.owner.getPet(this.my_first_pet.getName()));
        assertEquals(this.my_second_pet, this.owner.getPet(this.my_second_pet.getName()));
    }
    @Test public void test_new_pet()
    {
        assertNull(this.owner.getPet(this.my_second_pet.getName(), true));
    }
    @Test public void test_not_new_pet()
    {
        assertNotNull(this.owner.getPet(this.my_first_pet.getName(), true));
    }
    @Test public void test_pet_not_exist()
    {
        assertNull(this.owner.getPet("another_pet"));
    }
}
