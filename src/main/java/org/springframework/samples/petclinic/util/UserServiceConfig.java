package org.springframework.samples.petclinic.util;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.service.UserService;
import org.springframework.samples.petclinic.service.UserServiceImpl;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Configuration
@ComponentScan(value = { "org.springframework.samples.petclinic.service.UserServiceImpl" })
@Profile("test")
public class UserServiceConfig {
    @Bean
    public UserService userService() {
        return new UserServiceImpl();
    }

    @Bean
    public UserRepository userRepository() {
        return new UserRepositoryStub();
    }

    @Repository
    public class UserRepositoryStub implements UserRepository {

        private ArrayList<User> users = new ArrayList<>();

        public void save(User user) throws DataAccessException {
            this.users.add(user);
        }

        public int getUsersSize() {
            return this.users.size();
        }
    }
}
